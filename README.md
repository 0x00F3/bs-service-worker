# bs-service-worker

This package will *eventually* be the home of BuckleScript bindings for the
[JavaScript service worker API](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API). It is currently in a very early stage of development.

## Installation
`npm install bs-service-worker`

## Usage
This is meant to be a tight binding around the JavaScript API, and is intended
to feel idiomatic to JavaScript

## Implemented
- [X] [AbstractWorker](https://developer.mozilla.org/en-US/docs/Web/API/Worker)
- [x] [Cache](https://developer.mozilla.org/en-US/docs/Web/API/Cache)
- [x] [CacheMatchOptions](https://developer.mozilla.org/en-US/docs/Web/API/Cache/match)
- [x] [CacheDeleteOptions](https://developer.mozilla.org/en-US/docs/Web/API/Cache/delete)
- [x] [CacheKeysOptions](https://developer.mozilla.org/en-US/docs/Web/API/Cache/keys)
- [x] [CacheStorage](https://developer.mozilla.org/en-US/docs/Web/API/CacheStorageJ)
- [x] [CacheStorageOptions](https://developer.mozilla.org/en-US/docs/Web/API/CacheStorage/match)
- [x] [Client](https://developer.mozilla.org/en-US/docs/Web/API/Client)
- [x] [ClientsMatchAllOptions](https://developer.mozilla.org/en-US/docs/Web/API/Clients/matchAll)
- [x] [Clients](https://developer.mozilla.org/en-US/docs/Web/API/Clients)
- [x] [ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent) 
- [x] [FetchEvent](https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent)
- [ ] InstallEvent
- [ ] [MessageEvent](https://developer.mozilla.org/en-US/docs/Web/API/MessageEvent)
- [ ] [MessagePort?](https://developer.mozilla.org/en-US/docs/Web/API/MessagePort)(Do I really need this one?)
- [ ] Navigator.serviceWorker
- [ ] NotificationEvent
- [ ] ServiceWorker
- [ ] ServiceWorkerContainer
- [ ] ServiceWorkerGlobalScope
- [ ] ServiceWorkerRegistration
- [ ] SyncEvent
- [ ] SyncManager
- [ ] [Worker](https://developer.mozilla.org/en-US/docs/Web/API/Worker)
- [X] [WorkerCredentials](https://developer.mozilla.org/en-US/docs/Web/API/Worker/Worker)
- [ ] [WorkerOptions](https://developer.mozilla.org/en-US/docs/Web/API/Worker/Worker)
- [X] [WorkerType](https://developer.mozilla.org/en-US/docs/Web/API/Worker/Worker)
- [x] [WindowClient](https://developer.mozilla.org/en-US/docs/Web/API/WindowClient)

## Notes
### on EventTargets
I'm hoping to crack the code for how to perfectly enforce types onto 
`addEventListener`... someday. I haven't done it yet. For now, stick to the 
function-valued properties. Those are typed pretty well.
