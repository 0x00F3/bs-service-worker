open ExtendableEvent;
open Fetch;


type _fetchEvent('a);
type fetchEvent_like('a) = extendableEvent_like(_fetchEvent('a));
type fetchEvent = fetchEvent_like(Dom._baseClass);

/* mdn calls this constructor "rarely used." */
[@bs.new] external make : fetchEvent = "FetchEvent";

/* properties */
[@bs.get] external clientId: fetchEvent => string  = "clientId";
[@bs.get] external preloadResponse: fetchEvent => Js.Promise.t(Response.t) =
  "preloadResponse";
[@bs.get] external replacesClientId: fetchEvent => string = "replacesClientId";
[@bs.get] external resultingClientId: fetchEvent => string =
  "resultingClientId";
[@bs.get] external request: fetchEvent => Request.t = "request";

/* methods */
[@bs.send] external respondWith: fetchEvent => Js.Promise.t(Response.t) =
  "respondWith";

// also inherits waitUntil from ExtendableEvent.

/* expose t for ease of use */
type t = fetchEvent;