open Fetch;

type cache;

module Private = {
  module Delete = {
    [@bs.send]
    external withOptions:
      (cache, Request.t, CacheDeleteOptions.t) => Js.Promise.t(bool) =
      "delete";

    [@bs.send]
    external withoutOptions: (cache, Request.t) => Js.Promise.t(bool) =
      "delete";
  };

  module Keys = {
    module WithRequest = {
      [@bs.send]
      external withoutOptions:
        (cache, Request.t) => Js.Promise.t(list(Request.t)) =
        "keys";

      [@bs.send]
      external withOptions:
        (cache, Request.t, CacheMatchOptions.t) =>
        Js.Promise.t(list(Request.t)) =
        "keys";
    };

    module WithoutRequest = {
      [@bs.send]
      external withoutOptions: cache => Js.Promise.t(list(Request.t)) =
        "keys";

      [@bs.send]
      external withOptions:
        (cache, CacheMatchOptions.t) => Js.Promise.t(list(Request.t)) =
        "keys";
    };
  };
  module Match = {
    [@bs.send]
    external withoutOptions: (cache, Request.t)
      => Js.Promise.t(option(Response.t))
      = "match";

    [@bs.send]
    external withOptions: (cache, Request.t, CacheMatchOptions.t)
      => Js.Promise.t(option(Response.t))
      = "match";
  };

  module MatchAll = {
    module WithRequest = {
      [@bs.send]
      external withoutOptions:
        (cache, Request.t) => Js.Promise.t(list(Response.t)) =
        "match";

      [@bs.send]
      external withOptions:
        (cache, Request.t, CacheMatchOptions.t) =>
        Js.Promise.t(list(Response.t)) =
        "match";
    };

    module WithoutRequest = {
      [@bs.send]
      external withoutOptions: cache => Js.Promise.t(list(Response.t)) =
        "match";

      [@bs.send]
      external withOptions:
        (cache, CacheMatchOptions.t) => Js.Promise.t(list(Response.t)) =
        "match";
    };
  };
};

let match = (cache, ~options=?, ~req: Request.t):
  Js.Promise.t(option(Response.t)) => {
  switch (options) {
  | None => Private.Match.withoutOptions(cache, req)
  | Some(o) => Private.Match.withOptions(cache, req, o)
  };
};

let matchAll = (~options=?, ~req=?, cache): Js.Promise.t(list(Response.t)) => {
  switch (req) {
  | None =>
    switch (options) {
    | None => Private.MatchAll.WithoutRequest.withoutOptions(cache)
    | Some(o) => Private.MatchAll.WithoutRequest.withOptions(cache, o)
    }
  | Some(r) =>
    switch (options) {
    | None => Private.MatchAll.WithRequest.withoutOptions(cache, r)
    | Some(o) => Private.MatchAll.WithRequest.withOptions(cache, r, o)
    }
  };
};

[@bs.send] external add: (cache, Request.t) => Js.Promise.t(unit) = "add";

[@bs.send]
external addAll: (cache, list(Request.t)) => Js.Promise.t(unit) = "addAll";

[@bs.send]
external put: (cache, Request.t, Response.t) => Js.Promise.t(unit) = "put";

let delete = (cache, ~options=?, ~req: Request.t): Js.Promise.t(bool) => {
  switch (options) {
  | None => Private.Delete.withoutOptions(cache, req)
  | Some(o) => Private.Delete.withOptions(cache, req, o)
  };
};

let keys = (~options=?, ~req=?, cache): Js.Promise.t(list(Request.t)) => {
  switch (req) {
  | None =>
    switch (options) {
    | None => Private.Keys.WithoutRequest.withoutOptions(cache)
    | Some(o) => Private.Keys.WithoutRequest.withOptions(cache, o)
    }
  | Some(r) =>
    switch (options) {
    | None => Private.Keys.WithRequest.withoutOptions(cache, r)
    | Some(o) => Private.Keys.WithRequest.withOptions(cache, r, o)
    }
  };
};