type clients;

module Private = {
  module MatchAll = {
    [@bs.send]
    external withOptions: (clients, ClientsMatchAllOptions.t)
      => Js.Promise.t(list(Client.t))
      = "matchAll";

    [@bs.send]
    external withoutOptions: (clients)
      => Js.Promise.t(list(Client.t))
      = "matchAll";
  };
};

[@bs.send]
external get: (clients, string) => Js.Promise.t(Client.t) = "get";

let matchAll = (~options=?, clients): Js.Promise.t(list(Client.t))
=> {
  switch (options) {
  | None => Private.MatchAll.withoutOptions(clients)
  | Some(o) => Private.MatchAll.withOptions(clients, o)
  };
};

[@bs.send]
external openWindow: (clients, string)
  => Js.Promise.t(option(WindowClient.t))
  = "openWindow";

[@bs.send] external claim: (clients) => Js.Promise.t(unit) = "claim";

