open Fetch;

type cacheStorage;

module Private = {
  module Match = {
    [@bs.send]
    external withOptions:
      (cacheStorage, Request.t, CacheStorageOptions.t)
      => Js.Promise.t(option(Response.t))
      = "delete";

    [@bs.send]
    external withoutOptions: (cacheStorage, Request.t)
      => Js.Promise.t(option(Response.t))
      = "delete";
  }
}     

let match = (cacheStorage, ~options=?, ~req: Request.t):
  Js.Promise.t(option(Response.t)) => {
  switch (options) {
  | None => Private.Match.withoutOptions(cacheStorage, req)
  | Some(o) => Private.Match.withOptions(cacheStorage, req, o)
  };
};

[@bs.send]
external has: (cacheStorage, ~cacheName: string) => Js.Promise.t(bool)
  = "has";

[@bs.send]
external open_: (cacheStorage, ~cacheName: string)
  => Js.Promise.t(bool)
  = "open";

[@bs.send]
external delete: (cacheStorage, ~cacheName: string)
  => Js.Promise.t(bool)
  = "delete";

[@bs.send]
external keys: (cacheStorage) => Js.Promise.t(list(string)) = "keys";