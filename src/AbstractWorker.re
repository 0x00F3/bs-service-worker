type _abstractWorker('a);
type abstractWorker_like('a) = Dom.eventTarget_like(_abstractWorker('a));
type abstractWorker = abstractWorker_like(Dom._baseClass);

type errorHandler = Dom.errorEvent => unit;

[@bs.get]
external getOnError: (abstractWorker) => errorHandler
  = "onerror"; 
[@bs.set]
external setOnError: (abstractWorker, errorHandler) => unit
  = "onerror";


type t = abstractWorker;
type t_like('a) = abstractWorker;