[@bs.deriving jsConverter]
type t = [
  | `classic
  | [@bs.as "module"] `module_
];