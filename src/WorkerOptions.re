type t =
  { type_: WorkerType.t
  , credentials: WorkerCredentials.t
  , name: string
};