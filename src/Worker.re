type _worker('a);
type t_like('a) = AbstractWorker.t_like('a);
type t = t_like(Dom._baseClass);

module Private
{
  module Make
  {
    [@bs.new] external withoutOptions: string => t = "Worker";
    [@bs.new]
    external withOptions: (string, WorkerOptions.t) => t = "Worker";
  };
};

let make = (~options=?, ~aURL: string): t =>
{
  switch (options) {
  | None => Private.Make.withoutOptions(aURL)
  | Some(o) => Private.Make.withOptions(aURL, o)
  };
};

